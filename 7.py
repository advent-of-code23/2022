from typing import Optional, List, Dict

_input = open("7.txt")


class File:
    def __init__(self, name: str, size: int):
        self.name = name
        self.size = size


class Directory:
    def __init__(self, name: str, parent_directory: Optional["Directory"] = None):
        self.name = name
        self.parent = parent_directory
        self.dir_children: Dict[str, Directory] = {}
        self.file_children: List[File] = []

        self._total_size = None

    def add_child(self, child_input: str, _state: "State"):
        child_parsed = child_input.split(" ")

        if child_parsed[0] == "dir":
            directory = Directory(child_parsed[1], self)
            self.dir_children[directory.name] = directory
            _state.all_directories.append(directory)
        else:
            _file = File(child_parsed[1], int(child_parsed[0]))
            self.file_children.append(_file)

    def get_child_dir(self, name: str):
        return self.dir_children.get(name)

    @property
    def total_size(self):
        if self._total_size is None:
            self._total_size = 0
            for file_child in self.file_children:
                self._total_size += file_child.size
            for dir_child in self.dir_children.values():
                self._total_size += dir_child.total_size
        return self._total_size


class State:
    structure: Optional[Directory] = None
    current_directory: Optional[Directory] = None
    all_directories: List[Directory] = []

    def _cd(self, directory_path: str):
        if directory_path == "..":
            self.current_directory = self.current_directory.parent
        elif not self.structure:
            directory = Directory(directory_path)
            self.all_directories.append(directory)
            self.structure = self.current_directory = directory
        else:
            self.current_directory = self.current_directory.get_child_dir(directory_path)

    def run_command(self, cmd_input: str):
        cmd_parsed = cmd_input.split(" ")

        cmd = cmd_parsed[1]

        if cmd == "cd":
            self._cd(cmd_parsed[2])


state = State()

for line in _input:
    if line.startswith("$"):
        state.run_command(line.strip())
    else:
        state.current_directory.add_child(line.strip(), state)

sum_small_directories = 0
for directory in state.all_directories:
    if directory.total_size <= 100000:
        sum_small_directories += directory.total_size

_input.close()

print(f"Result 1: {sum_small_directories}")

TOTAL_SPACE = 70000000
NEEDED_FREE_SPACE = 30000000
CURRENT_SPACE_FILLED = state.structure.total_size
CURRENT_FREE_SPACE = TOTAL_SPACE - CURRENT_SPACE_FILLED
SPACE_TO_FREE_UP = NEEDED_FREE_SPACE - CURRENT_FREE_SPACE

space_diff = SPACE_TO_FREE_UP
space_diff_dir = None
for directory in state.all_directories:
    if directory.total_size < SPACE_TO_FREE_UP:
        continue

    diff = directory.total_size - SPACE_TO_FREE_UP
    if diff < space_diff:
        space_diff = diff
        space_diff_dir = directory

print(f"Result 2: {space_diff_dir.total_size}")
