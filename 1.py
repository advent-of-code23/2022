_input = open("1.txt")

ELVES = []

current_elf = []
for line in _input:
    if not line or line == "\n":
        ELVES.append(current_elf)
        current_elf = []
    else:
        current_elf.append(int(line))

_input.close()

highest_elves = [0] * 3
for elf in ELVES:
    elf_total = sum(elf)

    lowest_highest_elf = min(highest_elves)
    if elf_total > lowest_highest_elf:
        i = highest_elves.index(lowest_highest_elf)
        highest_elves[i] = elf_total

print(f"Result 1: {max(highest_elves)}")

print(f"Result 2: {sum(highest_elves)}")
