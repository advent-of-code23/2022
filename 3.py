from string import ascii_lowercase, ascii_uppercase
from typing import List

_input = open("3.txt")

PRIORITIES = ascii_lowercase + ascii_uppercase


class Rucksack:
    def __init__(self, _input: str):
        comp_size = int(len(_input) / 2)
        self.contents = {*_input}
        self.comp1 = {*_input[:comp_size]}
        self.comp2 = {*_input[comp_size:]}

    def get_both(self):
        return self.comp1.intersection(self.comp2).pop()

    @classmethod
    def get_priority(cls, char: str):
        return PRIORITIES.index(char) + 1

    @classmethod
    def get_badge(cls, rucksacks: List["Rucksack"]):
        intersection = None

        for _rucksack in rucksacks:
            if intersection is None:
                intersection = _rucksack.contents
            else:
                intersection = intersection & _rucksack.contents

        return intersection.pop()


total_priorities = 0
total_badge_priorities = 0
elf_group = []
for line in _input:
    rucksack = Rucksack(line.strip())
    both = rucksack.get_both()

    total_priorities += rucksack.get_priority(both)

    elf_group.append(rucksack)
    if len(elf_group) == 3:
        badge = rucksack.get_badge(elf_group)
        total_badge_priorities += rucksack.get_priority(badge)
        elf_group = []

_input.close()

print(f"Result 1: {total_priorities}")

print(f"Result 2: {total_badge_priorities}")
