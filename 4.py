_input = open("4.txt")


class Assignment:
    def __init__(self, assignment_input: str):
        start, end = assignment_input.split("-")
        self.start = int(start)
        self.end = int(end)


class Pair:
    def __init__(self, pair_input: str):
        one, two = pair_input.split(",")
        self.one = Assignment(one)
        self.two = Assignment(two)

    def _a_contains_b(self, a: Assignment, b: Assignment) -> bool:
        return a.start <= b.start <= a.end and a.start <= b.end <= a.end

    def _a_overlaps_b(self, a: Assignment, b: Assignment) -> bool:
        return a.start <= b.start <= a.end or a.start <= b.end <= a.end

    def is_contained(self) -> bool:
        return self._a_contains_b(self.one, self.two) or self._a_contains_b(self.two, self.one)

    def has_overlap(self) -> bool:
        return self._a_overlaps_b(self.one, self.two) or self._a_overlaps_b(self.two, self.one)


total_contains = 0
total_overlap = 0
for line in _input:
    pair = Pair(line.strip())

    if pair.is_contained():
        total_contains += 1
    if pair.has_overlap():
        total_overlap += 1

_input.close()

print(f"Result 1: {total_contains}")

print(f"Result 2: {total_overlap}")
