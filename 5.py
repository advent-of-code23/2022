from typing import List

_input = open("5.txt")

_input_lines = _input.readlines()
_input_lines_grid = _input_lines[:8]
_input_lines_moves = _input_lines[10:]

_input.close()


class Crate:
    def __init__(self, crate_input: str):
        self.id = crate_input[1]

    def __repr__(self):
        return f"{self.__class__.__name__}({self.id})"


class Grid:
    def __init__(self, cols: int):
        self.stacks: List[List[Crate]] = []
        for _ in range(cols):
            self.stacks.append([])

    def add_line_to_grid(self, line_input: str):
        j = 0
        while j < len(line_input):
            crate_input = line_input[j:j+3].strip()
            if crate_input:
                crate = Crate(crate_input)
                self.stacks[int(j / 4)].append(crate)
            j += 4

    def do_move(self, move_input: str, one_by_one: bool = True):
        move_parsed = move_input.split(" ")

        amount = int(move_parsed[1])
        from_col_i = int(move_parsed[3]) - 1
        to_col_i = int(move_parsed[5]) - 1

        crates = self.stacks[from_col_i][-amount:]
        if one_by_one:
            crates.reverse()
        self.stacks[to_col_i] += crates
        del self.stacks[from_col_i][-amount:]


grid = Grid(9)
grid_9001 = Grid(9)

_input_lines_grid.reverse()
for line in _input_lines_grid:
    grid.add_line_to_grid(line)
    grid_9001.add_line_to_grid(line)

for line in _input_lines_moves:
    grid.do_move(line.strip())
    grid_9001.do_move(line.strip(), False)

top_crates = ""
for column in grid.stacks:
    top_crates += column[-1].id

print(f"Result 1: {top_crates}")

top_crates_9001 = ""
for column in grid_9001.stacks:
    top_crates_9001 += column[-1].id

print(f"Result 2: {top_crates_9001}")
