_input = open("6.txt")

datastream = _input.readline().strip()

_input.close()


class Scanner:
    def __init__(self, stream: str):
        self.stream = stream

    def _first_marker_for_length(self, length: int):
        for i in range(len(self.stream)):
            if i < length - 1:
                continue

            char_i = i + 1
            chars = set(self.stream[char_i-length:char_i])
            if len(chars) == length:
                return i + 1

    def first_packet_marker_index(self):
        return self._first_marker_for_length(4)

    def first_messages_marker_index(self):
        return self._first_marker_for_length(14)


scanner = Scanner(datastream)
first_packet_marker_index = scanner.first_packet_marker_index()

print(f"Result 1: {first_packet_marker_index}")

first_message_marker_index = scanner.first_messages_marker_index()

print(f"Result 2: {first_message_marker_index}")
