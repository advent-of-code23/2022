from enum import Enum
from typing import Dict

_input = open("2.txt")


class Score(Enum):
    ROCK = 1
    PAPER = 2
    SCISSORS = 3

    LOST = 0
    DRAW = 3
    WIN = 6

    @classmethod
    def round(cls, opponent: "Score", me: "Score"):
        if opponent == cls.ROCK:
            if me == cls.ROCK:
                return cls.DRAW
            elif me == cls.PAPER:
                return cls.WIN
            elif me == cls.SCISSORS:
                return cls.LOST
        elif opponent == cls.PAPER:
            if me == cls.ROCK:
                return cls.LOST
            elif me == cls.PAPER:
                return cls.DRAW
            elif me == cls.SCISSORS:
                return cls.WIN
        elif opponent == cls.SCISSORS:
            if me == cls.ROCK:
                return cls.WIN
            elif me == cls.PAPER:
                return cls.LOST
            elif me == cls.SCISSORS:
                return cls.DRAW

    @classmethod
    def round_expected(cls, opponent: "Score", me: "Score"):
        if opponent == cls.ROCK:
            if me == cls.LOST:
                return cls.SCISSORS
            elif me == cls.DRAW:
                return cls.ROCK
            elif me == cls.WIN:
                return cls.PAPER
        elif opponent == cls.PAPER:
            if me == cls.LOST:
                return cls.ROCK
            elif me == cls.DRAW:
                return cls.PAPER
            elif me == cls.WIN:
                return cls.SCISSORS
        elif opponent == cls.SCISSORS:
            if me == cls.LOST:
                return cls.PAPER
            elif me == cls.DRAW:
                return cls.SCISSORS
            elif me == cls.WIN:
                return cls.ROCK


OPPONENT: Dict[str, Score] = {
    "A": Score.ROCK,
    "B": Score.PAPER,
    "C": Score.SCISSORS,
}

ME: Dict[str, Score] = {
    "X": Score.ROCK,
    "Y": Score.PAPER,
    "Z": Score.SCISSORS,
}

ME_EXPECTED: Dict[str, Score] = {
    "X": Score.LOST,
    "Y": Score.DRAW,
    "Z": Score.WIN,
}

total_points = 0
total_points_expected = 0
for line in _input:
    _opponent, _me = line.strip().split(" ")

    me_shape = ME[_me]
    result = Score.round(OPPONENT[_opponent], me_shape)
    total_points += me_shape.value + result.value

    me_shape_expected = ME_EXPECTED[_me]
    result = Score.round_expected(OPPONENT[_opponent], me_shape_expected)
    total_points_expected += me_shape_expected.value + result.value

_input.close()

print(f"Result 1: {total_points}")

print(f"Result 2: {total_points_expected}")
